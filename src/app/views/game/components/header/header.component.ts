import { Component, OnInit, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GameService } from '../../../../shared/services/game.service';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'app-header',
  standalone: true,
  imports: [CommonModule, RouterLink],
  templateUrl: './header.component.html',
  styleUrl: './header.component.scss'
})
export class HeaderComponent implements OnInit {
  
  private readonly gameService = inject(GameService);

  complet = this.gameService.grilleComplete;
  
  numberClick = this.gameService.numberClick;
  timer: string = '00:00';
  secondes: number = 0;
  
  ngOnInit(): void {
    this.startTimer();
  }

  startTimer() {
    setInterval(() => {
      if(!this.complet()) {
        this.timer = this.formatTimer();
        this.secondes++;
      }
    }, 1000);
  }

  formatTimer() {
    const minutes = Math.floor(this.secondes / 60);
    const seconds = this.secondes % 60;

    return `${minutes < 10 ? '0' + minutes : minutes}:${seconds < 10 ? '0' + seconds : seconds}`;
  }
}
