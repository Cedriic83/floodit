import { Component, inject } from '@angular/core';
import { GameService } from '../../../../shared/services/game.service';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-couleurs',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './couleurs.component.html',
  styleUrl: './couleurs.component.scss'
})
export class CouleursComponent {

  private readonly gameService = inject(GameService);
  colorSelected = this.gameService.colorSelected;
  listeCouleur = this.gameService.listeCouleur;

  selectColor(color: string): void {
    this.gameService.updateColorSelected(color);
  }

  classColorSelected(color: string): string {
    if(color === this.colorSelected()) {
      return 'selected';
    }
    return '';
  }
}
