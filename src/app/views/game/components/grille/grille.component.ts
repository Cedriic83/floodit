import { Component, inject } from '@angular/core';
import { GameService } from '../../../../shared/services/game.service';
import { CommonModule } from '@angular/common';
import { Grille } from '../../../../shared/model/grille.model';
import { RouterLink } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';

@Component({
  selector: 'app-grille',
  standalone: true,
  imports: [CommonModule, RouterLink, MatButtonModule],
  templateUrl: './grille.component.html',
  styleUrl: './grille.component.scss'
})
export class GrilleComponent {
  private readonly gameService = inject(GameService);
  grille = this.gameService.grille;
  tailleGrille = this.gameService.tailleGrille;
  grilleComplete = this.gameService.grilleComplete;

  ngOnInit(): void {
    this.gameService.creationGrille();
  }

  selectCase(grille: Grille): void {
    this.gameService.updateColorCible(grille.color);
    this.gameService.incrementeClick();
    this.gameService.changeCouleur(grille);
    console.log(this.grilleComplete())
  }

  creationTailleGrille(): any {
    return `grid-template-columns: repeat(${this.tailleGrille().x}, 1fr)`;
    
  }

  creationTailleBloc(): any {
    const size = 80 / this.tailleGrille().x;
    return `${size}vh`;
  }
}
