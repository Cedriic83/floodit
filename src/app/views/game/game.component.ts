import { Component, OnInit, inject } from '@angular/core';
import { HeaderComponent } from './components/header/header.component';
import { GrilleComponent } from './components/grille/grille.component';
import { CouleursComponent } from './components/couleurs/couleurs.component';
import { GameService } from '../../shared/services/game.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-game',
  standalone: true,
  imports: [HeaderComponent, GrilleComponent, CouleursComponent],
  templateUrl: './game.component.html',
  styleUrl: './game.component.scss'
})
export class GameComponent implements OnInit{

  private readonly gameService = inject(GameService);
  private readonly router = inject(Router);


  tailleGrille = this.gameService.tailleGrille;

  ngOnInit(): void {
    if(this.tailleGrille().x === 0) {
      this.router.navigate(['']);
    }
  }

}
