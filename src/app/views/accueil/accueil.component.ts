import { Component, OnInit, inject } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { RouterLink } from '@angular/router';
import { GameService } from '../../shared/services/game.service';


@Component({
  selector: 'app-accueil',
  standalone: true,
  imports: [MatButtonModule, RouterLink,],
  templateUrl: './accueil.component.html',
  styleUrl: './accueil.component.scss'
})
export class AccueilComponent implements OnInit {
  
  private readonly gameService = inject(GameService);
  
  listeDifficulte = [
    {label: 'Facile (10*10)', x: 10, y: 10},
    {label: 'Normal (20*20)', x: 20, y: 20},
    {label: 'Difficile (30*30)', x: 30, y: 30},
  ];

  ngOnInit(): void {
    this.gameService.grilleComplete.set(false);
    this.gameService.numberClick.set(0);
  }

  select(x: number, y: number): void {
    this.gameService.updateTailleGrille({x, y});
  }

}
