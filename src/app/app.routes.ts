import { Routes } from '@angular/router';

export const routes: Routes = [
  {
    path: '',
    loadComponent: () => import('./views/accueil/accueil.component').then(c => c.AccueilComponent)
  },

  {
    path: 'game',
    loadComponent: () => import('./views/game/game.component').then(c => c.GameComponent)
  },
];
