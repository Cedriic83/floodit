import { Injectable, WritableSignal, signal } from '@angular/core';
import { Grille } from '../model/grille.model';
import { Color } from '../enums/color.enum';
import { TailleGrille } from '../model/taille.model';

@Injectable({
  providedIn: 'root'
})
export class GameService {

  colorSelected: WritableSignal<string> = signal('');
  colorCible: WritableSignal<string> = signal('');
  numberClick: WritableSignal<number> = signal(0);
  grille: WritableSignal<Grille[]> = signal([]);
  tailleGrille: WritableSignal<TailleGrille> = signal({x:0, y:0});
  grilleComplete: WritableSignal<boolean> = signal(false);
  listeCouleur: string[] = [ Color.COLOR1, Color.COLOR2, Color.COLOR3, Color.COLOR4, Color.COLOR5, Color.COLOR6 ];

  updateColorSelected(color: string): void {
    this.colorSelected.set(color);
  }

  updateColorCible(color: string): void {
    this.colorCible.set(color);
  }

  updateTailleGrille(taille: TailleGrille): void {
    this.tailleGrille.set(
      {
        x: taille.x,
        y: taille.y
      }
    );
  }

  creationGrille(): void {
    const longueurLigne = this.tailleGrille().x + 1;
    const nombreLigne = this.tailleGrille().y + 1;
    const grille: Grille[] = [];
    let id = 1;

    for (let line = 1; line < nombreLigne; line++) {

      for (let index = 1; index < longueurLigne; index++) {
        grille.push({
          id: id,
          x: index,
          y: line,
          color: this.couleurAleatoire()
        });
        id++;
      }
    
    }
    this.grille.set(grille);
  }

  couleurAleatoire(): string {
    const indexAleatoire = Math.round(Math.random() * 5);
    return this.listeCouleur[indexAleatoire];
  }

  incrementeClick(): void {
    if(this.colorSelected() !== '') {
      this.numberClick.set(this.numberClick()+ 1);
    }
  }

  changeCouleur(caseGrille: Grille): void {
    const grille = this.grille();

    //setTimeout(() => {
    if(this.colorSelected() !== '' && this.colorSelected() !== this.colorCible()) {
      //modification de la case sélectionnée
      const index = grille.findIndex(e => e.id === caseGrille.id);
      grille[index].color = this.colorSelected();

      //modification de la case au dessus si besoin
      const indexDessus = grille.findIndex(e => e.y === caseGrille.y - 1 && e.x === caseGrille.x);
      if(indexDessus !== -1 && grille[indexDessus].color === this.colorCible()) {
        this.changeCouleur(grille[indexDessus]);
      }

      //modification de la case à droite si besoin
      const indexDroite = grille.findIndex(e => e.x === caseGrille.x + 1 && e.y === caseGrille.y);
      if(indexDroite !== -1 && grille[indexDroite].color === this.colorCible()) {
        this.changeCouleur(grille[indexDroite]);
      }

      //modification de la case à gauche si besoin
      const indexGauche = grille.findIndex(e => e.x === caseGrille.x - 1 && e.y === caseGrille.y);
      if(indexGauche !== -1 && grille[indexGauche].color === this.colorCible()) {
        this.changeCouleur(grille[indexGauche]);
      }
      

      //modification de la case au dessous si besoin
      const indexDessous = grille.findIndex(e => e.y === caseGrille.y + 1 && e.x === caseGrille.x);
      if(indexDessous !== -1 && grille[indexDessous].color === this.colorCible()) {
        this.changeCouleur(grille[indexDessous]);
      }
    }

    this.grille.set(grille);
    this.controleGrilleComlete();
  //}, 100);
  }

  private controleGrilleComlete() {
    const grille = this.grille();
    let etat = true;
    grille.forEach(block => {
      if(this.colorSelected() !== block.color) {
        etat = false;
        return;
      }
    });

    this.grilleComplete.set(etat);
  }
  
}
