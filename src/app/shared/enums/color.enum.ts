export enum Color {
  COLOR1 = '#d5dbdb',
  COLOR2 = '#52be80',
  COLOR3 = '#ec7063',
  COLOR4 = '#f5b041',
  COLOR5 = '#2e86c1',
  COLOR6 = '#9b59b6'
}