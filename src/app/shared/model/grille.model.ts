export interface Grille {
  id: number;
  y: number;
  x: number;
  color: string;
}